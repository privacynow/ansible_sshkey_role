describe command('lsb_release -a') do
  its('stdout') { should match (/Ubuntu/) }
end

userlist = ['bcarpio', 'bob', 'marry', 'mike','jane']
userlist.each do |user|
  describe user("#{user}") do
    it { should exist }
  end
end
