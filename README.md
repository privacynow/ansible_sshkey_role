# Ansible Role: ansible_sshkey_role

This Role Manages SSH Keys

## Using the role
### Installation
```
ansible-galaxy install ansible_sshkey_role
```

### Example Playbook
```
  - hosts: all
    roles:
      - ansible_sshkey_role
```

### Variables

See [`variables/main.yml`](variables/main.yml).

## Local Development / Testing using Vagrant
```
export KITCHEN_YAML=.kitchen.vagrant.yml
```

### Dependencies
- Bundler 1.13.0+
- Ruby 2.3.0+
- Docker 1.12.0+
- Vagrant
- Virtualbox
- See [`Gemfile`](Gemfile)
